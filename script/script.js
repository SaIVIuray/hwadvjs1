class Employee {
    constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary;
    }
  
    get getName() {
      return this.name;
    }
  
    set setName(newName) {
      this.name = newName;
    }
  
    get getAge() {
      return this.age;
    }
  
    set setAge(newAge) {
      this.age = newAge;
    }
  
    get getSalary() {
      return this.salary;
    }
  
    set setSalary(newSalary) {
      this.salary = newSalary;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    get getSalary() {
      return this.salary * 3;
    }
  }
  
  const programmer1 = new Programmer('Іван', 25, 50000, ['JavaScript', 'Python']);
  const programmer2 = new Programmer('Марія', 30, 60000, ['Java', 'C++']);
  
  console.log("Програміст 1:", programmer1.getName, programmer1.getAge, programmer1.getSalary, programmer1.lang);
  console.log("Програміст 2:", programmer2.getName, programmer2.getAge, programmer2.getSalary, programmer2.lang);

